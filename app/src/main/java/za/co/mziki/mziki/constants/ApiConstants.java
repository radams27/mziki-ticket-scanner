package za.co.mziki.mziki.constants;

/**
 * Created by reza on 2014-10-28.
 */
public class ApiConstants {

    public final static String API_LOGIN_URL = "http://mziiki.bnry.co.za/api/login";
    public final static String TICKET_REDEEM_URL = "http://mziiki.bnry.co.za/api/redeem_ticket";
    public final static String UNDO_REDEEM_TICKET_URL = "http://mziiki.bnry.co.za/api/undo_ticket_redeem";
    public final static String API_LOGOUT_URL = "http://mziiki.bnry.co.za/api/logout";
    public final static String API_CHECK_LOGIN = "http://mziiki.bnry.co.za/api/check_login";
}
