package za.co.mziki.mziki.model;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import za.co.mziki.mziki.R;

/**
 * Created by reza on 2014-10-28.
 */
public class AppModel extends Application{

    public static final String TAG = AppModel.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private static AppModel mInstance;
    private String ticketMasterID;

    private String name;
    private String _currentTicketID;
    private String _currentHash;



    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppModel getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void clearTicketData()
    {
        _currentHash = "";
        _currentTicketID = "";
    }

    public void saveTicketMasterID(String ticketMasterID,String name, Activity activity)
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.ticket_master_id),ticketMasterID);
        editor.putString(getString(R.string.username),name);
        editor.commit();
    }

    public Bundle savedDetails(Activity activity)
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        String ticketMasterID = sharedPref.getString(getString(R.string.ticket_master_id), "");
        String username = sharedPref.getString(getString(R.string.username), "");

        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.ticket_master_id),ticketMasterID);
        bundle.putString(getString(R.string.username),username);

        return bundle;
    }

    public void clearTicketMasterID(Activity activity)
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.ticket_master_id),"");
        editor.putString(getString(R.string.username),"");
        editor.commit();
    }

    public void setCurrentTicketID(String currentTicketID)
    {
        _currentTicketID = currentTicketID;
    }

    public void setCurrentHash(String currentHash)
    {
        _currentHash = currentHash;
    }

    public String getCurrentTicketID()
    {
        return _currentTicketID;
    }

    public String getCurrentHash()
    {
        return _currentHash;
    }


}
