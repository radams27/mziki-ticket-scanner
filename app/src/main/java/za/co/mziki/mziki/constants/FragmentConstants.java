package za.co.mziki.mziki.constants;

/**
 * Created by reza on 2014-10-20.
 */
public class FragmentConstants {

    public final static String LOGIN_CLICKED = "za.co.mziki.constants.FragmentConstants.LOGIN_CLICKED";
    public final static String FORGOT_PASSWORD_CLICKED = "za.co.mziki.constants.FragmentConstants.FORGOT_PASSWORD_CLICKED";

    public final static String SCAN_CLICKED = "za.co.mziki.constants.FragmentConstants.SCAN_CLICKED";

    public final static String NEXT_CLICKED = "za.co.mziki.constants.FragmentConstants.NEXT_CLICKED";
    public final static String TICKET_SUCCESS_CLOSE_CLICKED = "za.co.mziki.constants.FragmentConstants.TICKET_SUCCESS_CLOSE_CLICKED";
    public final static String TICKET_SUCCESS_UNDO_CLICKED = "za.co.mziki.constants.FragmentConstants.TICKET_SUCCESS_UNDO_CLICKED";

    public final static String SUBMIT_MANUAL_CODE_CLICKED = "za.co.mziki.constants.FragmentConstants.SUBMIT_MANUAL_CODE_CLICKED";
    public final static String TICKET_USED_CLOSED_CLICK = "za.co.mziki.constants.FragmentConstants.TICKET_USED_CLOSED_CLICK";

    public final static String INVALID_CODE_TRY_AGAIN_CLICK = "za.co.mziki.constants.FragmentConstants.INVALID_CODE_TRY_AGAIN_CLICK";
    public final static String INVALID_CODE_CANCEL_CLICK = "za.co.mziki.constants.FragmentConstants.INVALID_CODE_CANCEL_CLICK";




}
