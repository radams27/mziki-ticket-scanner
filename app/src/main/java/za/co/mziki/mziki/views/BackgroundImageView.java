package za.co.mziki.mziki.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.loaders.ImageLoader;

/**
 * Created by reza on 2014-10-23.
 */
public class BackgroundImageView extends ImageView {

    private ImageLoader.ViewHolder _viewHolder;
    private Context _ctx;

    public BackgroundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _ctx = context;

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.BackgroundImageView,
                0, 0);



        _viewHolder = new ImageLoader.ViewHolder();
        _viewHolder.id = 3;
        _viewHolder.drawable = a.getDrawable(R.styleable.BackgroundImageView_bg_src);
        _viewHolder.icon = this;


    }

    public void loadBgImage()
    {
        ImageLoader task = new ImageLoader(_viewHolder.icon, _ctx);
        task.execute(_viewHolder);
    }

    public void clearBitmap()
    {
        setImageBitmap(null);
    }

}
