package za.co.mziki.mziki.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.model.AppModel;
import za.co.mziki.mziki.views.BackgroundImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ManualInputFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ManualInputFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ManualInputFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;
    private BackgroundImageView _bg;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManualInputFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManualInputFragment newInstance(String param1, String param2) {
        ManualInputFragment fragment = new ManualInputFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public ManualInputFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manual_input, container, false);
        _bg = (BackgroundImageView) view.findViewById(R.id.manual_ticket_bg_holder);

        TextView title = (TextView) getActivity().findViewById(R.id.action_title);
        ImageButton closeBtn = (ImageButton) getActivity().findViewById(R.id.close_btn);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT );

        params.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        params.leftMargin = closeBtn.getWidth() + 10;

        title.setText("Type the code in");
        title.setLayoutParams(params);

        Button submitBtn = (Button) view.findViewById(R.id.manual_submit_btn);
        submitBtn.setOnClickListener(onClickListener);

        return view;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {

                Bundle bundle = new Bundle();
                bundle.putString("type", FragmentConstants.SUBMIT_MANUAL_CODE_CLICKED);
                bundle.putBoolean("isTicketManual", true);

                EditText manualCodeInput = (EditText) getView().findViewById(R.id.manual_code_input);
                EditText manualEmailInput = (EditText) getView().findViewById(R.id.manual_email_input);
                EditText manualCellInput = (EditText) getView().findViewById(R.id.manual_cell_input);

                AppModel.getInstance().setCurrentHash(manualCodeInput.getText().toString());
                bundle.putString("fallback_code", manualCodeInput.getText().toString());
                bundle.putString("email", manualEmailInput.getText().toString());
                bundle.putString("cell", manualCellInput.getText().toString());
                mListener.onFragmentInteraction(bundle);
            }

        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        _bg.clearBitmap();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _bg.loadBgImage();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Bundle uri);
    }

}
