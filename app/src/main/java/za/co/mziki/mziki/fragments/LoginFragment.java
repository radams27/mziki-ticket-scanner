package za.co.mziki.mziki.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.loaders.ImageLoader;
import za.co.mziki.mziki.views.BackgroundImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Button loginBtn;
    private Button forgotPasswordBtn;
    private BackgroundImageView _bg;
    private TextView _errorText;
    private View _loginBubble;
    private ScrollView _scrollView;
    ImageLoader.ViewHolder v;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_login, container, false);

        _bg = (BackgroundImageView)view.findViewById(R.id.login_bg_holder);

        ImageView imageview = new ImageView(getActivity());
       // FrameLayout fl =

        //_bg.setBackgroundColor(Color.WHITE);

       /* _bg = (ImageView)view.findViewById(R.id.login_bg_holder);
        SVG svg = new SVGBuilder()
                .readFromResource(getResources(), R.raw.recordplayer).build();
        Picture picture = svg.getPicture();
        Drawable drawable = svg.getDrawable();
        _bg.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        _bg.setImageDrawable(drawable);*/

        /*try{
        SVG  svg = SVG.getFromResource(getResources(), R.raw.recordplayer);
        // Create a canvas to draw onto
        if (svg.getDocumentWidth() != -1) {
            Bitmap newBM = Bitmap.createBitmap(200,
                    200,
                    Bitmap.Config.ARGB_8888);
            Canvas bmcanvas = new Canvas(newBM);

            // Clear background to white
            bmcanvas.drawRGB(255, 255, 255);
            // Render our document onto our canvas
            svg.renderToCanvas(bmcanvas);
        }}
        catch(SVGParseException e){

        }*/


        loginBtn = (Button) view.findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mListener != null) {
                    Bundle bundle = new Bundle();
                    EditText ticketIdTxt = (EditText) view.findViewById(R.id.ticket_master_id_input);
                    EditText passwordTxt = (EditText) view.findViewById(R.id.password_input);


                    bundle.putString("type", FragmentConstants.LOGIN_CLICKED);
                    bundle.putString("user", ticketIdTxt.getText().toString());
                    bundle.putString("password", passwordTxt.getText().toString());

                    mListener.onFragmentInteraction(bundle);
                }
            }
        });

        TextView title = (TextView) getActivity().findViewById(R.id.action_title);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        params.addRule(RelativeLayout.CENTER_VERTICAL);

        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.leftMargin = 90;
        params.alignWithParent = true;

        title.setText("Hello! Please login");
        title.setLayoutParams(params);

        _errorText = (TextView) view.findViewById(R.id.login_bubble_text);



        _loginBubble = view.findViewById(R.id.login_bubble);
        _loginBubble.setVisibility(View.INVISIBLE);


        /*scrollView.setOnTouchListener( new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                Log.d("touch","df");
                return true;
            }
        });*/
        //_scrollView.setEnabled(false);

        // Request focus and show soft keyboard automatically
        //editText.requestFocus();
        //getWindow().setSoftInputMode(LinearLayout.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return view;
    }

    public void showError(String error)
    {
        _loginBubble.setVisibility(View.VISIBLE);
        _errorText.setText(error);
    }

    public void hideError()
    {
        _loginBubble.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        _bg.clearBitmap();

    }

    @Override
    public void onResume()
    {
        super.onResume();
        _bg.loadBgImage();
        //_scrollView.setEnabled(true);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Bundle uri);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();


    }








}
