package za.co.mziki.mziki.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import za.co.mziki.mziki.MainActivity;

/**
 * Created by reza on 2014-10-29.
 */
public class ApiService  {

    private JSONObject _jsonObject;
    private Context _ctx;
    public MainActivity.SuccessResponse successResponse;
    public MainActivity.ErrorResponse errorResponse;


    public ApiService(Context ctx)
    {
        _ctx = ctx;
    }

    public void getRequest(JSONObject json,String apiRequest)
    {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                apiRequest, json,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.d("response", response.toString());
                        successResponse.success(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("error", "Error: " + error.getMessage());
                        errorResponse.error(error);

                    }
                });

        jsonObjReq.setShouldCache(false);
        AppModel.getInstance().addToRequestQueue(jsonObjReq);

    }

    public interface SuccessResponse
    {
        void success(JSONObject json);
    }

    public interface ErrorResponse
    {
        void error(VolleyError error);
    }


}
