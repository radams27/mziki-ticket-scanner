package za.co.mziki.mziki;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.mziki.mziki.constants.ApiConstants;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.fragments.InvalidCodeFragment;
import za.co.mziki.mziki.fragments.LoginFragment;
import za.co.mziki.mziki.fragments.ManualInputFragment;
import za.co.mziki.mziki.fragments.ScanFragment;
import za.co.mziki.mziki.fragments.TicketSuccessFragment;
import za.co.mziki.mziki.fragments.TicketUsedFragment;
import za.co.mziki.mziki.model.ApiService;
import za.co.mziki.mziki.model.AppModel;


public class MainActivity extends Activity implements
        LoginFragment.OnFragmentInteractionListener,
        ScanFragment.OnFragmentInteractionListener,
        TicketSuccessFragment.OnFragmentInteractionListener,
        ManualInputFragment.OnFragmentInteractionListener,
        TicketUsedFragment.OnFragmentInteractionListener,
        InvalidCodeFragment.OnFragmentInteractionListener
       {


    private ImageButton _closeBtn;
    private LoginFragment loginFragment;
    private ActionBar _actionBar;
    private Menu _menu;
    private MenuItem _menuItem;
    private ProgressDialog _progressDialog;
    private int _currentFragmentID;
    private boolean _isDoubleBackPressed = false;
    private boolean _isTicketManual = false;
    private ScrollView _scrollView;
    private boolean _isKeyPadOpen = false;
    boolean trigger = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _progressDialog = new ProgressDialog(this);
        _progressDialog.setMessage("Loading...");

        _actionBar = getActionBar();
        _actionBar.setDisplayShowHomeEnabled(false);
        //displaying custom ActionBar
        View mActionBarView = getLayoutInflater().inflate(R.layout.action_bar,null,true);

        _actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        _actionBar.setCustomView(mActionBarView);
        _actionBar.getCustomView().findViewById(R.id.close_btn);

        _closeBtn = (ImageButton)_actionBar.getCustomView().findViewById(R.id.close_btn);

        checkLogin();


        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }
        }

        _scrollView =(ScrollView) findViewById(R.id.scroll_view);
        _scrollView.setOnTouchListener( new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                Log.d("touch","df");
                if(_isKeyPadOpen)
                {
                    return false;
                }else {
                    return true;
                }
            }
        });

        final FrameLayout view = (FrameLayout)findViewById(R.id.main_activity);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            @Override
            public void onGlobalLayout() {
                if ((view.getRootView().getHeight() - view.getHeight()) >
                        view.getRootView().getHeight()/3) {

                    // keyboard is open
                    _isKeyPadOpen = true;
                    _scrollView.setVerticalScrollBarEnabled(true);

                } else {

                    // keyboard is closed
                    _isKeyPadOpen = false;
                    _scrollView.setVerticalScrollBarEnabled(false);

                }
            }
        });


    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.w("barcode >>> ",contents.toString());
                _isTicketManual = false;
                //save public hash for later use in case user wants to undo
                AppModel.getInstance().setCurrentHash(contents.toString());
                getHashDetails(contents.toString());


                // Handle successful scan
            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
                Log.i("App","Scan unsuccessful");
            }
        }
    }

    public void onClose(View v)
    {
        logout();
    }

    public void onFragmentInteraction(Bundle bundle)
    {

        String type = bundle.getString("type");
        switch(type)
        {
            case FragmentConstants.LOGIN_CLICKED:

                login(bundle.getString("user"),bundle.getString("password"));
                break;

            case FragmentConstants.FORGOT_PASSWORD_CLICKED:
                break;

            case FragmentConstants.SCAN_CLICKED:
                initScan();

                break;
            case FragmentConstants.NEXT_CLICKED:
                initScan();

                break;

            case FragmentConstants.SUBMIT_MANUAL_CODE_CLICKED:
                _isTicketManual = bundle.getBoolean("isTicketManual");
                getFallbackCodeDetais(bundle.getString("fallback_code"),bundle.getString("email"),bundle.getString("cell"));
                break;

            case FragmentConstants.TICKET_USED_CLOSED_CLICK:
                addFragment(new ScanFragment(),true);

                break;
            case FragmentConstants.INVALID_CODE_TRY_AGAIN_CLICK:
                initScan();

                break;
            case FragmentConstants.INVALID_CODE_CANCEL_CLICK:
                addFragment(new ScanFragment(),true);

                break;

            case FragmentConstants.TICKET_SUCCESS_CLOSE_CLICKED:
                addFragment(new ScanFragment(),true);

                break;

            case FragmentConstants.TICKET_SUCCESS_UNDO_CLICKED:
                undoRedeemTicket();

                break;
        }

        if(_scrollView != null) {
            _scrollView.setVerticalScrollBarEnabled(false);
            _scrollView.scrollTo(0, 0);
        }
  }

    private void initScan()
    {

         Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, 0);
    }

    private void checkLogin()
    {
        Fragment firstFragment;

        boolean isLoggedin = false;
        Bundle savedDetails = AppModel.getInstance().savedDetails(this);
        String ticketMasterID = savedDetails.getString(getString(R.string.ticket_master_id));

        if(ticketMasterID.equals(""))
        {
            firstFragment = new LoginFragment();
            Log.d("not logged in", AppModel.getInstance().savedDetails(this).getString(getString(R.string.ticket_master_id)));
        }
        else
        {
            firstFragment = new ScanFragment();
            Log.d("already logged in", AppModel.getInstance().savedDetails(this).getString(getString(R.string.ticket_master_id)));
            isLoggedin = true;
        }

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        firstFragment.setArguments(getIntent().getExtras());

        // Add the fragment to the 'fragment_container' FrameLayout
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.add(R.id.fragment_container, firstFragment);
        transaction.disallowAddToBackStack();

        transaction.commit();
        _currentFragmentID = firstFragment.getId();

        if(!isLoggedin)
        {
             _closeBtn.setVisibility(View.GONE);
            editIconVisible(false);

        }else
        {
            editIconVisible(true);
        }

    }

    private void logout()
    {
        _progressDialog.setMessage("Logging out...");
        _progressDialog.show();

            JSONObject json = new JSONObject();

            ApiService apiService = new ApiService(this);
            apiService.getRequest(json, ApiConstants.API_LOGOUT_URL);
            apiService.successResponse = new SuccessResponse() {
                @Override
                public void success(JSONObject json) {
                    _progressDialog.hide();
                    try {
                        int success = json.getInt("success");
                        if (success == 1) {
                            addFragment(new LoginFragment(), false);
                            _closeBtn.setVisibility(View.GONE);
                            editIconVisible(false);

                            AppModel.getInstance().clearTicketMasterID(MainActivity.this);
                        } else {
                            Log.d("error", ">>");
                        }
                    } catch (JSONException e) {
                        Log.d("error", e.toString());
                    }

                }
            };

            apiService.errorResponse = new ErrorResponse() {
                @Override
                public void error(VolleyError error) {
                    _progressDialog.hide();
                }
            };


    }

    private void undoRedeemTicket()
    {
        _progressDialog.setMessage("Undoing...");
        _progressDialog.show();
        try {
            JSONObject json = new JSONObject();
            json.put("ticket_id", AppModel.getInstance().getCurrentTicketID());
            Log.d("ticket id",AppModel.getInstance().getCurrentTicketID());
            if(_isTicketManual)
            {
                json.put("fallback_code",  AppModel.getInstance().getCurrentHash());
                Log.d("fallback_code",AppModel.getInstance().getCurrentHash());
            }else
            {
                json.put("public_hash",  AppModel.getInstance().getCurrentHash());
                Log.d("public_hash",AppModel.getInstance().getCurrentHash());
            }




            ApiService apiService = new ApiService(this);
            apiService.getRequest(json, ApiConstants.UNDO_REDEEM_TICKET_URL);
            apiService.successResponse = new SuccessResponse() {
                @Override
                public void success(JSONObject json)
                {
                    _progressDialog.hide();
                    try
                    {
                        int success = json.getInt("success");
                        if (success == 1)
                        {
                            addFragment(new ScanFragment(),true);
                            AppModel.getInstance().clearTicketData();
                            //reset flag
                            _isTicketManual = false;
                        }
                        else
                        {
                            Log.d("error",">>");
                            Toast.makeText(MainActivity.this, "Undo was unsuccessful", Toast.LENGTH_SHORT).show();
                        }
                    }catch(JSONException e)
                    {
                        Log.d("error",e.toString());
                    }

                }
            };
            apiService.errorResponse = new ErrorResponse() {
                @Override
                public void error(VolleyError error)
                {
                    _progressDialog.hide();
                    Log.d("error",error.toString());
                }
            };


        }catch(JSONException e){
            _progressDialog.hide();

        }


    }


    private void getFallbackCodeDetais(String fallbackCode, String email, String cell)
    {
        _progressDialog.setMessage("Loading...");
        editIconVisible(true);
        _progressDialog.show();

        try {
            JSONObject json = new JSONObject();
            json.put("fallback_code", fallbackCode);
            json.put("email", email);
            json.put("cell", cell);
            ApiService apiService = new ApiService(this);
            apiService.getRequest(json, ApiConstants.TICKET_REDEEM_URL);
            apiService.successResponse = new SuccessResponse() {
                @Override
                public void success(JSONObject json)
                {

                    _progressDialog.hide();
                    Bundle bundle = new Bundle();
                    bundle.putString("json", json.toString());
                    hideKeyboard();


                    Fragment fragment;
                    try {
                        JSONObject data = json.getJSONObject("data");
                        AppModel.getInstance().setCurrentTicketID(data.getString("ticket_id"));

                        String ticketState = data.getString("ticket_state");
                        if(ticketState.equals("valid"))
                        {
                            fragment = new TicketSuccessFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, true);
                            Log.d(">valid<",json.toString());
                        }else if(ticketState.equals("redeemed"))
                        {
                            fragment = new TicketUsedFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, false);
                            Log.d(">used<",json.toString());
                        }else{
                            fragment = new InvalidCodeFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, false);
                            Log.d(">invalid<",json.toString());
                        }
                    }catch(JSONException e){
                        Log.d("error",e.toString());

                    }

                }
            };
            apiService.errorResponse = new ErrorResponse() {
                @Override
                public void error(VolleyError error) {
                    _progressDialog.hide();

                }
            };

        }
        catch(JSONException e)
        {
            _progressDialog.hide();
            Log.d("error",e.toString());
        }
    }

    private void getHashDetails(final String hash)
    {
        _progressDialog.setMessage("Loading...");
        editIconVisible(true);
        _progressDialog.show();

        try {
            JSONObject json = new JSONObject();
            json.put("public_hash", hash);
            ApiService apiService = new ApiService(this);
            apiService.getRequest(json, ApiConstants.TICKET_REDEEM_URL);
            apiService.successResponse = new SuccessResponse() {
                @Override
                public void success(JSONObject json)
                {
                    _progressDialog.hide();

                    Bundle bundle = new Bundle();
                    Log.d(">hash response<",json.toString());
                    bundle.putString("json",json.toString());
                    Fragment fragment;
                    try {
                        JSONObject data = json.getJSONObject("data");
                        AppModel.getInstance().setCurrentTicketID(data.getString("ticket_id"));

                        String ticketState = data.getString("ticket_state");
                        if(ticketState.equals("valid"))
                        {
                            fragment = new TicketSuccessFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, true);
                            Log.d(">valid<",json.toString());
                        }else if(ticketState.equals("redeemed"))
                        {
                            fragment = new TicketUsedFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, true);
                            Log.d(">used<",json.toString());
                        }else{
                            fragment = new InvalidCodeFragment();
                            fragment.setArguments(bundle);
                            addFragment(fragment, true);
                            Log.d(">invalid<",json.toString());
                        }
                    }catch(JSONException e){
                        Log.d("error",e.toString());

                    }



                }
            };
            apiService.errorResponse = new ErrorResponse() {
                @Override
                public void error(VolleyError error) {
                    _progressDialog.hide();
                }
            };

        }
        catch(JSONException e)
        {
            _progressDialog.hide();
            Log.d("error",e.toString());
        }
    }

    private void login(final String ticketMasterNumber, final String password)
    {
        _progressDialog.setMessage("Logging in...");
        _progressDialog.show();
        try
        {
            JSONObject json = new JSONObject();
            json.put("ticket_master_number", ticketMasterNumber);
            json.put("ticket_master_password", password);
            ApiService apiService = new ApiService(this);
            apiService.getRequest(json, ApiConstants.API_LOGIN_URL);

            apiService.successResponse = new SuccessResponse()
            {
                @Override
                public void success(JSONObject json)
                {
                    _progressDialog.hide();
                    try
                    {
                        int success = json.getInt("success");
                        if (success == 1) {

                            JSONObject data = json.getJSONObject("data");
                            String name = data.getString("name");
                            addFragment(new ScanFragment(), true);
                            _closeBtn.setVisibility(View.VISIBLE);
                            editIconVisible(true);
                            AppModel.getInstance().saveTicketMasterID(ticketMasterNumber,name,MainActivity.this);
                            //hide keyboard
                            hideKeyboard();
                        } else {
                            LoginFragment loginFrag = (LoginFragment) getFragmentManager().findFragmentById(_currentFragmentID);
                            AppModel.getInstance().clearTicketMasterID(MainActivity.this);
                            if (loginFrag != null) {
                                loginFrag.showError("Please enter a valid TicketMaster ID and Password");
                            }
                        }
                    }
                    catch(JSONException e)
                    {
                        Log.d("json", "error");
                    }
                }
            };

            apiService.errorResponse = new ErrorResponse()
            {
                @Override
                public void error(VolleyError error)
                {
                    _progressDialog.hide();
                    LoginFragment loginFrag = (LoginFragment) getFragmentManager().findFragmentById(_currentFragmentID);
                    if (loginFrag != null) {
                        loginFrag.showError("Error connecting. Try again");
                    }
                    Log.w("login error", error.toString());
                }
            };

        }
        catch(JSONException e)
        {
            _progressDialog.hide();
            Log.d("error",e.toString());
        }

    }

    public interface SuccessResponse
    {
        void success(JSONObject json);
    }

    public interface ErrorResponse
    {
       void error(VolleyError error);
    }

    private void hideKeyboard()
    {
        View view = getCurrentFocus();
        if(view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            _isKeyPadOpen = false;
        }
    }

    private void addFragment(Fragment fragment, boolean isStacked)
    {
        // Create new fragment and transaction
        _progressDialog.hide();
        _isKeyPadOpen = false;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
        transaction.setCustomAnimations(R.anim.enter_anim, R.anim.exit_anim);


        transaction.replace(R.id.fragment_container, fragment);
        transaction.disallowAddToBackStack();
        if(isStacked) {
            //transaction.addToBackStack(null);
        }

// Commit the transaction

        transaction.commit();
        _currentFragmentID = fragment.getId();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.manual_input_icon) {
             addFragment(new ManualInputFragment(),true);

            _closeBtn.setVisibility(View.VISIBLE);
            editIconVisible(false);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

   public void editIconVisible(boolean isTrigger) {
       trigger = isTrigger;
       invalidateOptionsMenu();
   }

   @Override
   public boolean onPrepareOptionsMenu(Menu menu)
   {
       _menu = menu;
       _menuItem = _menu.findItem(R.id.manual_input_icon);
       if(trigger) {

           _menuItem.setVisible(true);
           //trigger = false;
       }else
       {
           _menuItem.setVisible(false);
       }
       return super.onPrepareOptionsMenu(menu);
   }

           @Override
           public void onBackPressed() {
               if (_isDoubleBackPressed) {
                   //prevent leak
                   _progressDialog.dismiss();
                   finish();
                   super.onBackPressed();
                   return;
               }

               _isDoubleBackPressed = true;
               Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

               new android.os.Handler().postDelayed(new Runnable() {

                   @Override
                   public void run() {
                       _isDoubleBackPressed=false;
                   }
               }, 2000);
           }
       }
