package za.co.mziki.mziki.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.views.BackgroundImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketUsedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketUsedFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TicketUsedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private BackgroundImageView _bg;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TicketUsedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TicketUsedFragment newInstance(String param1, String param2) {
        TicketUsedFragment fragment = new TicketUsedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public TicketUsedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ticket_used, container, false);

        _bg = (BackgroundImageView) view.findViewById(R.id.ticket_used_bg);
        ImageButton actionBarCloseBtn = (ImageButton) getActivity().findViewById(R.id.close_btn);

        Button closeBtn = (Button) view.findViewById(R.id.used_ticket_close_btn);
        closeBtn.setOnClickListener(onClickListener);

        String json = getArguments().getString("json");

        JSONObject jsonParsed;
        try{
            jsonParsed = new JSONObject(json);
            JSONObject  data = jsonParsed.getJSONObject("data");

            TextView name = (TextView)view.findViewById(R.id.used_ticket_name);
            TextView email = (TextView)view.findViewById(R.id.used_ticket_email);
            TextView concert = (TextView)view.findViewById(R.id.used_ticket_concert);
            //TextView type = (TextView)view.findViewById(R.id.ticket_type);
            TextView cell = (TextView)view.findViewById(R.id.used_ticket_cell);
            TextView bubble = (TextView)view.findViewById(R.id.used_ticket_bubble_text);

            name.setText(data.getString("name"));
            email.setText(data.getString("email"));
            concert.setText(data.getString("concert"));
            //type.setText(data.getString("name"));
            cell.setText(data.getString("cell"));

            String[] parts = data.getString("redeemed_on").split("T");
            String date = parts[0]; // 004
            String time = parts[1]; // 034556

            String bubbleText = "Processed on " + date + " at " + time.substring(0,8) + " \n by " + data.getString("name");

            //String bubbleText = "Processed on " + data.getString("redeemed_on") + " \n by " + data.getString("name");
            bubble.setText(bubbleText);

            Log.d("name", data.getString("name"));
            Log.d("name",data.getString("email"));

        }catch(JSONException e)
        {
            Log.d("json", "error");
        }

        TextView title = (TextView) getActivity().findViewById(R.id.action_title);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT );

        params.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);

        params.leftMargin = actionBarCloseBtn.getLayoutParams().width + 10;

        //title.
        title.setText("Ticket already used");
        title.setLayoutParams(params);

        return view;

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {

                Bundle bundle = new Bundle();
                bundle.putString("type", FragmentConstants.TICKET_USED_CLOSED_CLICK);

                mListener.onFragmentInteraction(bundle);
            }

        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        _bg.clearBitmap();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _bg.loadBgImage();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Bundle uri);
    }

}
