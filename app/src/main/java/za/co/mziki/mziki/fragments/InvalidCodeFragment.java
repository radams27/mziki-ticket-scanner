package za.co.mziki.mziki.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.views.BackgroundImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InvalidCodeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InvalidCodeFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class InvalidCodeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private BackgroundImageView _bg;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InvalidCodeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InvalidCodeFragment newInstance(String param1, String param2) {
        InvalidCodeFragment fragment = new InvalidCodeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public InvalidCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_invalid_code, container, false);

        ImageButton closeBtn = (ImageButton) getActivity().findViewById(R.id.close_btn);
       _bg = (BackgroundImageView)view.findViewById(R.id.invalid_bg);

        Button try_again_btn = (Button)view.findViewById(R.id.try_again_btn);
        Button cancel_btn = (Button)view.findViewById(R.id.cancel_btn);

        try_again_btn.setOnClickListener(onTryAgainClickListener);
        cancel_btn.setOnClickListener(onCancelClickListener);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT );

        params.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        params.leftMargin = closeBtn.getLayoutParams().width + 10;



        return view;
    }

    private View.OnClickListener onTryAgainClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {

                Bundle bundle = new Bundle();
                bundle.putString("type", FragmentConstants.INVALID_CODE_TRY_AGAIN_CLICK);

                mListener.onFragmentInteraction(bundle);
            }

        }
    };

    private View.OnClickListener onCancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {

                Bundle bundle = new Bundle();
                bundle.putString("type", FragmentConstants.INVALID_CODE_CANCEL_CLICK);

                mListener.onFragmentInteraction(bundle);
            }

        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        _bg.clearBitmap();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _bg.loadBgImage();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Bundle uri);
    }

}
