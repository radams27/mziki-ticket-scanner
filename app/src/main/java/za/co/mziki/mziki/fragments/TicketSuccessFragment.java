package za.co.mziki.mziki.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import za.co.mziki.mziki.R;
import za.co.mziki.mziki.constants.FragmentConstants;
import za.co.mziki.mziki.loaders.ImageLoader;
import za.co.mziki.mziki.views.BackgroundImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketSuccessFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketSuccessFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TicketSuccessFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ImageLoader.ViewHolder v;
    private Button _nextBtn;
    private Button _undoBtn;
    private Button _closeBtn;
    private BackgroundImageView _bg;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TicketSuccessFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TicketSuccessFragment newInstance(String param1, String param2) {
        TicketSuccessFragment fragment = new TicketSuccessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public TicketSuccessFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view  = inflater.inflate(R.layout.fragment_ticket_success, container, false);

        _bg = (BackgroundImageView)view.findViewById(R.id.ticket_success_bg_holder);

        _nextBtn = (Button) view.findViewById(R.id.next_btn);
        _undoBtn = (Button) view.findViewById(R.id.ticket_undo_btn);
        _closeBtn = (Button) view.findViewById(R.id.ticket_close_btn);

        _nextBtn.setOnClickListener(onNextClickListener);
        _undoBtn.setOnClickListener(onUndoClickListener);
        _closeBtn.setOnClickListener(onCloseClickListener);

        String json = getArguments().getString("json");

        JSONObject jsonParsed;
        try{
            jsonParsed = new JSONObject(json);
            JSONObject  data = jsonParsed.getJSONObject("data");

            TextView name = (TextView)view.findViewById(R.id.ticket_name);
            TextView email = (TextView)view.findViewById(R.id.ticket_email);
            TextView concert = (TextView)view.findViewById(R.id.ticket_concert);
            //TextView type = (TextView)view.findViewById(R.id.ticket_type);
            TextView cell = (TextView)view.findViewById(R.id.ticket_cell);
            TextView bubble = (TextView)view.findViewById(R.id.ticket_bubble_text);

            name.setText(data.getString("name"));
            email.setText(data.getString("email"));
            concert.setText(data.getString("concert"));
            //type.setText(data.getString("name"));
            cell.setText(data.getString("cell"));


            //String string = "004-034556";
            String[] parts = data.getString("redeemed_on").split("T");
            String date = parts[0]; // 004
            String time = parts[1]; // 034556

            String bubbleText = "Processed on " + date + " at " + time.substring(0,8) + " \n by " + data.getString("name");
            bubble.setText(bubbleText);

            Log.d("name",data.getString("name"));
            Log.d("name",data.getString("email"));

        }catch(JSONException e)
        {
            Log.d("json", "error");
        }

        TextView title = (TextView) getActivity().findViewById(R.id.action_title);
        ImageButton closeBtn = (ImageButton) getActivity().findViewById(R.id.close_btn);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT );

        params.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
        params.leftMargin = closeBtn.getLayoutParams().width + 10;



        title.setText("Ticket successful");
        title.setLayoutParams(params);


        return view;
    }

    private View.OnClickListener onNextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                Bundle bundle = new Bundle();
                bundle.putString("type",FragmentConstants.NEXT_CLICKED);
                mListener.onFragmentInteraction(bundle);
            }

        }
    };

    private View.OnClickListener onCloseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                Bundle bundle = new Bundle();
                bundle.putString("type",FragmentConstants.TICKET_SUCCESS_CLOSE_CLICKED);
                mListener.onFragmentInteraction(bundle);
            }

        }
    };

    private View.OnClickListener onUndoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                Bundle bundle = new Bundle();
                bundle.putString("type",FragmentConstants.TICKET_SUCCESS_UNDO_CLICKED);
                mListener.onFragmentInteraction(bundle);
            }

        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        _bg.clearBitmap();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        _bg.loadBgImage();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Bundle uri);
    }

}
