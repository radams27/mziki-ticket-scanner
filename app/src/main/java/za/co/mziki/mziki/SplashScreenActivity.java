package za.co.mziki.mziki;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        String fontPath = "fonts/RobotoSlab-Light.ttf";
        // text view label
        TextView txtGhost = (TextView) findViewById(R.id.splashText);


        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
        txtGhost.setTypeface(tf);



        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
               // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                //mTextField.setText("done!");
                Intent intent = new Intent(SplashScreenActivity.this,MainActivity.class);
                startActivity(intent);

                ImageView img1 = (ImageView) findViewById(R.id.white_logo);
                ImageView img2 = (ImageView) findViewById(R.id.money_img);

                Bitmap bmp1 =   ((BitmapDrawable) img1.getDrawable()).getBitmap();
                Bitmap bmp2 =   ((BitmapDrawable) img2.getDrawable()).getBitmap();
                bmp1.recycle();
                bmp2.recycle();


                ((BitmapDrawable) img2.getDrawable()).getBitmap().recycle();
                finish();
        }
        }.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
