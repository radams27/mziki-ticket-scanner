package za.co.mziki.mziki.loaders;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.LruCache;
import android.view.Display;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;

import java.lang.ref.WeakReference;

/**
 * Created by reza on 2014-10-21.
 */


public class ImageLoader extends AsyncTask<ImageLoader.ViewHolder,String,Bitmap> {
    private SimpleCursorAdapter cAdapter;
    private Cursor cursorAsync;
    private int count = 0;

    private ImageLoader.ViewHolder v;
    private LruCache<String, Bitmap> mMemoryCache;
    private WeakReference<ImageView> imageViewReference;
    private Context _ctx;

    public ImageLoader(ImageView imageView, Context ctx) {
        _ctx = ctx;
        imageViewReference = new WeakReference<ImageView>(imageView);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    @Override
    protected void onPreExecute() {
        //adapter = (ArrayAdapter<ListItemVO>) list.getAdapter();
        //viewHolder.icon.setVisibility(View.INVISIBLE);
    }

    @Override
    protected Bitmap doInBackground(ImageLoader.ViewHolder... params) {
        v = params[0];

       // Bitmap bmp = BitmapUtils.setImageToImageView(v.imageUrl, 1024);
        Bitmap bmp = ((BitmapDrawable)v.drawable).getBitmap();
        //if (bmp != null)
            //addBitmapToMemoryCache(String.valueOf(v.id), bmp);
        return bmp;

    }

    @Override
    protected void onProgressUpdate(String... values) {
        //ListItemVO li = new ListItemVO(0,values[0],"","",values[1]);
        // adapter.add(li);
        //View v = list.getChildAt(count);
        //View newView = (View) cAdapter.getView(count,v,list);
        //TextView tv = (TextView) newView.findViewById(id.item_name);
        // ImageView pic = (ImageView) adapter.

        //Bitmap bmp = BitmapUtils.setImageToImageView(values[0]);

        //v.setImageBitmap(bmp);
        //pic.setImageBitmap(bmp);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        // Close the cursor to avoid a resource leak.
        super.onPostExecute(result);
        if (imageViewReference != null && result != null) {

            v.icon = imageViewReference.get();
            if (v.icon != null) {
                //Log.w("id", "" + v.position);
                //v.icon.setVisibility(View.VISIBLE);


                WindowManager wm = (WindowManager) _ctx.getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);


                v.icon.setImageBitmap(result);

                v.icon.setAdjustViewBounds(true);
                v.icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                v.icon.setAdjustViewBounds(true);
                v.icon.setImageAlpha(90);
                //v.icon.setClipBounds(new Rect(0,0,0,size.y));
                v.icon.setMaxHeight(size.y);


                Animation fadeIn = new AlphaAnimation(0, 1);  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
                fadeIn.setInterpolator(new AccelerateInterpolator());
                fadeIn.setStartOffset(0); // Start fading out after 500 milli seconds
                fadeIn.setDuration(300); // Fadeout duration should be 1000 milli seconds
                v.icon.setAnimation(fadeIn);
            }
        }
        //v.txt.setBackgroundColor(v.color);
        //v.icon.setBackgroundColor(v.color);


    }

   public static class ViewHolder {
       public long id;
       public ImageView icon;
       public String imageUrl;
       public Drawable drawable;
       public ProgressBar progress;
       public AsyncTask task;
    }
}


